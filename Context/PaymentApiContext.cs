using Microsoft.EntityFrameworkCore;
using src.Models;
using tech_test_payment_api.src.Models;

namespace PaymentApi.Context
{
    public class PaymentApiContext : DbContext
    {
        public PaymentApiContext(DbContextOptions<PaymentApiContext> options) : base(options)
        {

        }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder){
            builder.Entity<Vendedor>(tabela => {
                tabela.HasKey(e => e.Id);
                tabela.HasMany(e => e.vendas).WithOne().HasForeignKey(c => c.VendedorId);
            });
        }
    }
}