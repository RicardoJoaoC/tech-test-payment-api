using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Models;

namespace src.Models;

public class Vendedor
{
    public Vendedor()
    {
        this.Nome = "Template";
        this.Cpf = "";
        this.Email = "";
        this.vendas = new List<Venda>();
        this.Telefone = "";
    }
    public Vendedor(string Nome, int Id, string Cpf, string Email, string Telefone)
    {
        this.Nome = Nome;
        this.Cpf = Cpf;
        this.Email = Email;
        this.Telefone = Telefone;
        this.vendas = new List<Venda>();
    }
        public int? Id { get; set; }
        public string? Nome { get; set; }
        public string? Cpf { get; set; }
        public string? Email { get; set; }
        public string? Telefone { get; set; }
        public List<Venda> vendas { get; set; }
    }