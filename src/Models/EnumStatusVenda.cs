using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace tech_test_payment_api.src.Models
{
    public enum EnumStatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelado
    }
}