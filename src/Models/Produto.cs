using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using src.Models;

namespace tech_test_payment_api.src.Models
{

public class Produto
{
    public Produto()
    {
        this.Id = 0;
        this.Descricao = "Template";
        this.Valor = 0;
    }

    public Produto(int id, string Descricao, double Valor, EnumStatusVenda Status)
    {
        this.Id = Id;
        this.Descricao = Descricao;
        this.Valor = Valor;
    }
        [System.ComponentModel.DataAnnotations.Key()]
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public int IdProduto { get; set; }
        public int IdVendedor { get; set; }
    }
}