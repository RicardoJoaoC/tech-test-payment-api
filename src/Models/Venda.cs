using System.ComponentModel.DataAnnotations.Schema;
using src.Models;

namespace tech_test_payment_api.src.Models;


public class Venda
{
    public Venda()
    {
        this.DataCriacao = DateTime.Now;
        this.Valor = 0;
        this.Pago = false;
        this.produtos = new List<Produto>();
        this.Status = Status;
    }

    public Venda(string TokenId, double Valor)
    {
        this.DataCriacao = DateTime.Now;
        this.Valor = Valor;
        this.Pago = false;
        this.produtos = new List<Produto>();
        this.Status = Status;
    }
    public int Id { get; set; }
    public DateTime DataCriacao { get; set; }
    public double Valor { get; set; }
    public bool Pago { get; set; }
    public int VendedorId { get; set; }
    public List<Produto> produtos { get; set; }
    public EnumStatusVenda Status { get; set; }
}