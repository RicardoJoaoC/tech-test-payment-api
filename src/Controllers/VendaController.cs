using Microsoft.AspNetCore.Mvc;
using src.Models;
using tech_test_payment_api.src.Models;
using System.Collections.Generic;
using PaymentApi.Context;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace src.Controllers;

[ApiController]
[Route("[controller]")]
public class VendaController : ControllerBase 
{
    private PaymentApiContext _context { get; set; }
    public VendaController(PaymentApiContext context)
    {
        this._context = context;
    }

    [HttpPost("CriarNovaVenda")]
    public ActionResult<Venda> Post([FromBody]Venda venda)
    {
        try
        {
            _context.Add(venda);
            _context.SaveChanges();
        }catch (System.Exception)
        {
            return BadRequest(new { msg = "Impossível criar",
            status = HttpStatusCode.BadRequest });
        }
        return Created("Criado", venda);
    }

    [HttpGet]
    public ActionResult<List<Venda>> Get()
    {
        var result = _context.Vendedores.Include( p => p.vendas).ToList();

        if (!result.Any()) {
            return NoContent();
        }

        return Ok(result);
    }

    [HttpPut("{id}")]
    public ActionResult<Venda> Update([FromRoute]int id, [FromBody]Vendedor vendedor)
    {
        var result = _context.Vendedores.SingleOrDefault(e => e.Id == id);

        if (result is null){
            return NotFound(new { msg = "Não encontrado!",
            status = HttpStatusCode.NotFound });
        }

        try
        {
            _context.Vendedores.Update(result);
            _context.SaveChanges();
        }
        catch (SystemException)
        {
            return BadRequest( new { msg = $"Erro ao atualizar o ID {id}",
                    status = HttpStatusCode.BadRequest });
        }

        return Ok( new { msg = $"Dados do ID {id} atualizados",
                    status = HttpStatusCode.OK });
    }
        
    [HttpDelete("{id}")]
    public ActionResult<Object> Delete([FromRoute]int id){
        var result = _context.Vendas.SingleOrDefault(e => e.Id == id);

        if (result == null){
            return BadRequest(new { msg = "Não encontrado!",
                    status = HttpStatusCode.BadRequest });
        }

        _context.Vendas.Remove(result);
        _context.SaveChanges();

        return Ok(new { msg = $"Deletado com sucesso o ID: {id}",
                    status = HttpStatusCode.OK });
    }
}